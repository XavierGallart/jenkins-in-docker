# Jenkins-in-docker

Docker compose file to start Jenkins in docker

Usage:
```
docker network create jenkins
docker volume create jenkins-docker-certs
docker volume create jenkins-data
docker volume create jenkins-home

docker-compose up -d
```